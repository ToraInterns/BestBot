package com.tora.hangoutsbot.networking;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.json.JsonFactory;

import com.google.api.services.admin.directory.DirectoryScopes;
import com.google.api.services.admin.directory.model.*;
import com.google.api.services.admin.directory.Directory;
import com.tora.hangoutsbot.csv.CsvParser;
import com.tora.hangoutsbot.start.Configuration;

import java.io.*;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

public class GoogleConnection implements Callable<Map<String, String>> {
    /** Global instance of the JSON factory. */
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    /** Global instance of the HTTP transport. */
    private static HttpTransport HTTP_TRANSPORT;

    /** Global instance of the scopes required by this quickstart. */
    private static final List<String> SCOPES =
            Arrays.asList(DirectoryScopes.ADMIN_DIRECTORY_USER_READONLY);
    
    static {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }
    
    public GoogleConnection() {

    }

    /**
     * Creates an authorized Credential object.
     * @return an authorized Credential object.
     * @throws IOException
     */
    public static Credential authorize() throws IOException, GeneralSecurityException  {
    	GoogleCredential credential = new GoogleCredential.Builder()
    			.setTransport(HTTP_TRANSPORT)
    		    .setJsonFactory(JSON_FACTORY)
    		    .setServiceAccountId(Configuration.SERVICE_ACCOUNT_EMAIL)
    		    .setServiceAccountUser(Configuration.SERVICE_ACCOUNT_USER)
    		    .setServiceAccountPrivateKeyFromP12File(new File(Configuration.P12KEY))
    		    .setServiceAccountScopes(SCOPES)
    		    .build();
    	credential.refreshToken();
		return credential;
    }

    /**
     * Build and return an authorized Admin SDK Directory client service.
     * @return an authorized Directory client service
     * @throws IOException
     * @throws GeneralSecurityException 
     */
    public static Directory getDirectoryService() throws IOException, GeneralSecurityException {
        Credential credential = authorize();
        return new Directory.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(Configuration.APPLICATION_NAME)
                .build();
    }

    @Override
    public Map<String,String> call() throws IOException, GeneralSecurityException {
        // Build a new authorized API client service.
        Directory service = getDirectoryService();

        // Get the first 500 users in the domain.
        Users result = service.users().list()
                .setMaxResults(500)
                .setOrderBy("givenname")
                .setViewType("domain_public")
                .setDomain(Configuration.API_DOMAIN)
                .execute();
        List<User> users = result.getUsers();
        CsvParser csvParser = new CsvParser("src/main/resources/user.csv");
        csvParser.generateCsvFile(users);
        return csvParser.read();
    }

}