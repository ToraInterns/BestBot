package com.tora.hangoutsbot.networking.exceptions;

/**
 * Created by intern on 11/10/15.
 */
public class XMPPLoginException extends Exception {

    public XMPPLoginException(String message, Throwable cause) {
        super(message, cause);
    }

    public XMPPLoginException(String message) {
        super(message);
    }
}