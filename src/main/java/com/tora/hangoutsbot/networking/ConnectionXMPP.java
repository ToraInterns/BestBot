package com.tora.hangoutsbot.networking;
import com.tora.hangoutsbot.bot.Bot;
import com.tora.hangoutsbot.networking.exceptions.XMPPLoginException;
import org.jivesoftware.smack.*;
import org.jivesoftware.smack.packet.Message;

import java.util.HashMap;

public final class ConnectionXMPP implements MessageListener, ConnectionListener, ConnectionInterface {

    public ConnectionXMPP(Bot bot) {
        this.bot = bot;
    }

    private Bot bot;
    private static final ConnectionConfiguration CONNECTION_CONFIG =
            new ConnectionConfiguration("talk.google.com", 5222, "gmail.com");
    private final XMPPConnection XMPP = new XMPPConnection(CONNECTION_CONFIG);
    private boolean connected;
    private HashMap<String, Chat> cache = new HashMap<String, Chat>();
    private String participant = "";

    public String getParticipant() {
        return this.participant;
    }

    public void connect() throws XMPPException {
        if (connected) {
            return;
        }
        XMPP.connect();
        XMPP.addConnectionListener(this);
        connected = true;
    }

    public void login(String username, String password) throws XMPPLoginException {
        if (!connected) {
            return;
        }
        try {
            XMPP.login(username, password);
            XMPP.getChatManager().addChatListener(new ChatManagerListener() {
                ;

                @Override
                public void chatCreated(Chat chat, boolean b) {
                    chat.addMessageListener(new MessageListener() {
                        @Override
                        public void processMessage(Chat chat, Message message) {
                            if(message.getBody() != null) {
                                String msg = "Received; " + message.getBody();
                                bot.receiveMessage(message.getBody(), chat.getParticipant(), ConnectionXMPP.this);
                            }
                            participant = chat.getParticipant();
                        }
                    });
                }
            });
            System.out.println("Bot waiting for user input.");
        } catch (XMPPException exception) {
            throw new XMPPLoginException("There was an error logging in! Are you using the correct username/password?", exception);
        }
    }

    public void sendPM(String message, String to) throws XMPPException {
        Chat c;
        String msg = "Msg to send: " + message;
        if (cache.containsKey(to))
            c = cache.get(to);
        else {
            c = XMPP.getChatManager().createChat(to, this);
            cache.put(to, c);
        }
        c.sendMessage(message);
    }

    public boolean isConnected() {
        return connected;
    }

    public void disconnect() {
        if (!connected)
            return;
        XMPP.disconnect();
        connected = false;
    }

    @Override
    public void processMessage(Chat arg0, Message arg1) {
    }

    @Override
    public void connectionClosed() {
        connected = false;
    }

    @Override
    public void connectionClosedOnError(Exception e) {
        connected = false;
    }

    @Override
    public void reconnectingIn(int seconds) {
    }

    @Override
    public void reconnectionFailed(Exception e) {
        if (connected)
            connected = false;
    }

    @Override
    public void reconnectionSuccessful() {
        if (!connected)
            connected = true;
    }

}