package com.tora.hangoutsbot.networking;
/**
 * Created by intern on 11/10/15.
 */
public interface ConnectionInterface {

    public void sendPM(String message, String to) throws Exception;

    public boolean isConnected();

    public void login(String username, String password) throws Exception;

    public void connect() throws Exception;

    public String getParticipant();

    public void disconnect();
}
