package com.tora.hangoutsbot.bot;

import com.tora.hangoutsbot.networking.ConnectionInterface;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Bot implements com.tora.hangoutsbot.bot.BotInterface {

    private Map<String, String> userMap;

    public void setUserMap(Map<String, String> userMap) {
        this.userMap = userMap;
    }

	@Override
	public void sendMessage(String message, String to, ConnectionInterface con) {
		try {
			con.sendPM(message, to);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void receiveMessage(String message, String from, ConnectionInterface con) {
		if (message != null) {
			List<String> contacts = searchByName(message);
			String size = " " + contacts.size();
			StringBuilder retMessage = new StringBuilder();
			if(contacts.size() == 1)
				retMessage.append("Found").append(size).append(" contact\n\n");
			else
				retMessage.append("Found").append(size).append(" contacts\n\n");

			for (String contact : contacts) {
				retMessage.append(contact).append("\n\n");
			}
			sendMessage(retMessage.toString(), from, con);
		}
	}

	private List<String> searchByName(String message) {
		List<String> contacts = new ArrayList<String>();
		for(Map.Entry<String, String> pair : userMap.entrySet()) {
			if(pair.getKey().toLowerCase().contains(message.toLowerCase()))
				contacts.add(pair.getKey() + " " + pair.getValue());
		}

		return contacts;
	}
}