package com.tora.hangoutsbot.bot;

import com.tora.hangoutsbot.networking.ConnectionInterface;

/**
 * Created by intern on 11/10/15.
 */
public interface BotInterface {

	public void sendMessage(String message, String to, ConnectionInterface con);

	public void receiveMessage(String message, String from, ConnectionInterface con);


}
