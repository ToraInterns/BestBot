package com.tora.hangoutsbot.csv;

import com.google.api.services.admin.directory.model.User;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by intern on 11/10/15.
 * A class used to parse a CSV file.
 */
public class CsvParser {
    private String fileName;
    private FileWriter writer;
    public CsvParser(String fileName) {
        this.fileName = fileName;
    }

    /**
     *
     * @param users list of users
     * @return true if succesfully generated the CSV file, false otherwise
     *
     */
    public boolean generateCsvFile(List<User> users) {
        try{
            writer = new FileWriter(fileName);
            for (User user : users) {
                writer.append(user.getName().getFullName());
                writer.append(",\tprimary email : " + user.getPrimaryEmail());
                List<String> prettyEmails = emails(user.getEmails(), user.getPrimaryEmail());
                if(prettyEmails.size() > 0) //append only if there are any secondary emails
                    writer.append(", secondary email(s) : ");
                for(String mail : prettyEmails)
                    writer.append(" " + mail);
                Object userPhone = user.getPhones();
                if(userPhone != null) {
                    Object phoneDetails = user.getPhones();
                    List<String> phones = userPhones(userPhone.toString());
                    writer.append(",\tPhone number(s): ");
                    for(String phone : phones) {
                        writer.append(phone + "     ");
                    }
                }
                writer.append("\n");
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;

    }

    /**
     *
     * @return a HashMap in which the key is the name of the user and the value contains informations
     * about the user like e-mail adress(es), telephone number.
     *
     * <br>If an error occured then an empty hash map is returned.
     */
    public HashMap<String,String> read() {
        HashMap<String,String> emptyHashMap = new HashMap<String, String>();
        HashMap<String ,String> map = new HashMap<String, String>();
        String line = "";
        String cvsSplitBy =",";
        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(fileName));
            while((line = br.readLine()) != null) {
                String[] word = line.split(cvsSplitBy);
                String name = word[0];
                String info = "";
                for (int i = 1; i < word.length; i++) {
                    info += " \n" + word[i];
                }
                map.put(name,info);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return emptyHashMap;
        } catch (IOException e) {
            e.printStackTrace();
            return emptyHashMap;
        }
        return map;
    }

    /**
     *
     * @param emails object containg informations about the emails
     * @param primaryMail a string containing the primary email
     *
     * @return a list with all the emails except for the primary email.
     */
    private List<String> emails(Object emails,String primaryMail) {
        List<String> emailList = new ArrayList<String>();
        String emailString = emails.toString();
        String[] splits = emailString.split(",");

        int i = 0;
        while(i < splits.length) {
            if(splits[i].contains(primaryMail)) {
                i += 2;
                continue;
            }
            String prettyEmail = splits[i].replace("[","");
            prettyEmail = prettyEmail.replace("{","");
            prettyEmail = prettyEmail.replace("]","");
            prettyEmail = prettyEmail.replace("}","");
            prettyEmail = prettyEmail.replace(" ","");
            prettyEmail = prettyEmail.replace("address=","");
            emailList.add(prettyEmail);
            i++;
        }
        return emailList;
    }

    private List<String> userPhones(String phoneDetails) {
        List<String> phones = new ArrayList<String>();
        int valueStartIndex = phoneDetails.indexOf("value");
        int valueEndIndex = phoneDetails.indexOf(",",valueStartIndex);
        int typeStartIndex = phoneDetails.indexOf("type");
        int typeEndIndex = phoneDetails.indexOf("}",typeStartIndex);

        while(valueStartIndex != -1) {

            String value = phoneDetails.substring(valueStartIndex+"value".length()+1, valueEndIndex);
            String type = phoneDetails.substring(typeStartIndex+"type".length()+1, typeEndIndex);
            String phone = type + ": " + value;
            phones.add(phone);
            valueStartIndex = phoneDetails.indexOf("value",typeEndIndex);
            valueEndIndex = phoneDetails.indexOf(",",valueStartIndex);
            typeStartIndex = phoneDetails.indexOf("type",valueEndIndex);
            typeEndIndex = phoneDetails.indexOf("}",typeStartIndex);
        }
        return phones;
    }
}