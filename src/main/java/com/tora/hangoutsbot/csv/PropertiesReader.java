package com.tora.hangoutsbot.csv;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {
	
	String filename;
	Properties properties;
	
	public PropertiesReader(String filename) {
		this.filename = filename;
		properties = new Properties();
	}
	
	public Properties readProperties() {
		try {
			InputStream inputStream = new FileInputStream(filename);
			properties.load(inputStream);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return properties;
	}
	
}