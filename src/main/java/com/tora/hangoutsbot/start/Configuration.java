package com.tora.hangoutsbot.start;

import com.tora.hangoutsbot.csv.PropertiesReader;

import java.util.Properties;

/**
 * @Author Adrian Muntean
 * Created by intern on 11/26/15.
 */
public class Configuration {
    /** Global instance of private key*/
    public static String P12KEY;

    /** Global instance of the service account email*/
    public static String SERVICE_ACCOUNT_EMAIL;

    public static String SERVICE_ACCOUNT_USER;

    /** Hangouts username*/
    public static String USERNAME;

    /** Hangouts password*/
    public static String PASSWORD;

    /** Hangouts nickname*/
    public static String NICKNAME;

    public static String APPLICATION_NAME;

    /** Application domain*/
    public static String API_DOMAIN;

    /**
     * Method which opens the configuration file and sets all the properties.
     * @param configFile
     */
    public static void sourceFile(String configFile) {
        PropertiesReader propReader = new PropertiesReader(configFile);
        Properties props = propReader.readProperties();
        P12KEY = props.getProperty("p12key");
        SERVICE_ACCOUNT_EMAIL = props.getProperty("service_account_email");
        USERNAME = props.getProperty("username");
        PASSWORD = props.getProperty("password");
        NICKNAME = props.getProperty("nickname");
        SERVICE_ACCOUNT_USER = props.getProperty("service_account_user");
        APPLICATION_NAME = props.getProperty("application_name");
        API_DOMAIN = props.getProperty("api_domain");
    }
}
