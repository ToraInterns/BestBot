package com.tora.hangoutsbot.start;

import com.tora.hangoutsbot.bot.Bot;
import com.tora.hangoutsbot.networking.ConnectionInterface;
import com.tora.hangoutsbot.networking.ConnectionXMPP;
import com.tora.hangoutsbot.networking.GoogleConnection;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Created by intern on 11/10/15.
 */
public class Main {
	/**
	 * This is the entry point for the application.
	 * 
	 * @param args
	 *            - the path for the config file
	 */
	public static void main(String[] args) {
		String configFile = "";
		if (args.length != 0) {
			configFile = args[0];
		} else {
			configFile = "src/main/resources/config.properties";
		}

		Configuration.sourceFile(configFile);
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		GoogleConnection googleConnection = new GoogleConnection();
		Future<Map<String, String>> userMap = executorService.submit(googleConnection);
		Bot myBot = new Bot();
        try {
            myBot.setUserMap(userMap.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        ConnectionInterface con = new ConnectionXMPP(myBot);
		try {
			con.connect();
			con.login(Configuration.USERNAME, Configuration.PASSWORD);
		} catch (Exception e) {
			e.printStackTrace();
		}
		//Updates the user map every 12 hours
		ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
		while (true) {
			Future<Map<String, String>> newUserMap = scheduledExecutorService.schedule(googleConnection, 12, TimeUnit.HOURS);
			try {
				myBot.setUserMap(newUserMap.get());
				System.out.println("User map is updated");
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
	}

}